//
//  DebugManager.h
//  Cubid
//
//  Created by Tuomo Syvänperä on 23.4.2015.
//

#pragma once

using namespace Urho3D;

class DebugManager : public Object {
    OBJECT(DebugManager);

public:
    DebugManager(Context* context);

    void Initialize();
    void CleanUp();

private:
    void HandleKeyDown(StringHash eventType, VariantMap& eventData);
};