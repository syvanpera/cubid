//
//  Block.h
//  Cubid
//
//  Created by Tuomo Syvänperä on 21.4.2015.
//

#pragma once

#include <array>

#include "Math/Vector3i.h"

static const std::array<std::array<std::array<Vector3i, 4>, 4>, 6> VertexConnectedBlocks = {{
    // Forward (0,0,1)
    {{
         {{Vector3i(0, 0, 1), Vector3i( 0, -1, 1), Vector3i( 1, -1, 1), Vector3i( 1,  0, 1)}},
         {{Vector3i(0, 0, 1), Vector3i( 1,  0, 1), Vector3i( 1,  1, 1), Vector3i( 0,  1, 1)}},
         {{Vector3i(0, 0, 1), Vector3i(-1,  0, 1), Vector3i(-1, -1, 1), Vector3i( 0, -1, 1)}},
         {{Vector3i(0, 0, 1), Vector3i( 0,  1, 1), Vector3i(-1,  1, 1), Vector3i(-1,  0, 1)}}
     }},
    // Right (1,0,0)
    {{
         {{Vector3i(1, 0, 0), Vector3i(1, -1,  0), Vector3i(1, -1, -1), Vector3i(1,  0, -1)}},
         {{Vector3i(1, 0, 0), Vector3i(1,  0, -1), Vector3i(1,  1, -1), Vector3i(1,  1,  0)}},
         {{Vector3i(1, 0, 0), Vector3i(1,  0,  1), Vector3i(1, -1,  1), Vector3i(1, -1,  0)}},
         {{Vector3i(1, 0, 0), Vector3i(1,  1,  0), Vector3i(1,  1,  1), Vector3i(1,  0,  1)}}
     }},
    // Back (0,0,1)
    {{
         {{Vector3i(0, 0, -1), Vector3i( 0, -1, -1), Vector3i(-1, -1, -1), Vector3i(-1,  0, -1)}},
         {{Vector3i(0, 0, -1), Vector3i(-1,  0, -1), Vector3i(-1,  1, -1), Vector3i( 0,  1, -1)}},
         {{Vector3i(0, 0, -1), Vector3i( 1,  0, -1), Vector3i( 1, -1, -1), Vector3i( 0, -1, -1)}},
         {{Vector3i(0, 0, -1), Vector3i( 0, -1, -1), Vector3i( 1, -1, -1), Vector3i( 1,  0, -1)}}
     }},
    // Left (-1,0,0)
    {{
         {{Vector3i(-1, 0, 0), Vector3i(-1, -1,  0), Vector3i(-1, -1,  1), Vector3i(-1,  0,  1)}},
         {{Vector3i(-1, 0, 0), Vector3i(-1,  0,  1), Vector3i(-1,  1,  1), Vector3i(-1,  1,  0)}},
         {{Vector3i(-1, 0, 0), Vector3i(-1,  0, -1), Vector3i(-1, -1, -1), Vector3i(-1, -1,  0)}},
         {{Vector3i(-1, 0, 0), Vector3i(-1,  1,  0), Vector3i(-1,  1, -1), Vector3i(-1,  0, -1)}}
     }},
    // Up (0,1,0)
    {{
         {{Vector3i(0, 1, 0), Vector3i( 0, 1, -1), Vector3i(-1, 1, -1), Vector3i(-1, 1,  0)}},
         {{Vector3i(0, 1, 0), Vector3i(-1, 1,  0), Vector3i(-1, 1,  1), Vector3i( 0, 1,  1)}},
         {{Vector3i(0, 1, 0), Vector3i( 1, 1,  0), Vector3i( 1, 1, -1), Vector3i( 0, 1, -1)}},
         {{Vector3i(0, 1, 0), Vector3i( 0, 1,  1), Vector3i( 0, 1,  1), Vector3i( 1, 1,  0)}}
     }},
    // Down (0,-1,0)
    {{
         {{Vector3i(0, -1, 0), Vector3i( 0, -1,  1), Vector3i(-1, -1,  1), Vector3i(-1, -1,  0)}},
         {{Vector3i(0, -1, 0), Vector3i(-1, -1,  0), Vector3i(-1, -1, -1), Vector3i( 0, -1, -1)}},
         {{Vector3i(0, -1, 0), Vector3i( 1, -1,  0), Vector3i( 1, -1, -1), Vector3i( 0, -1, -1)}},
         {{Vector3i(0, -1, 0), Vector3i( 0, -1, -1), Vector3i( 1, -1, -1), Vector3i( 1, -1,  0)}}
     }}
 }};


//
// Vertex order within a face:
//
// 1----3
// | \  |
// |  \ |
// |   \|
// 0----2
//

static const std::array<std::array<std::array<float, 16>, 4>, 6> BlockVertexData = {{
    // Forward (0,0,1)
    {{ // Vertex               Normal                 Color                      UV             Tangent
        {{1.0f, 0.0f, 1.0f,    0.0f,  0.0f,  1.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.0f, 1.0f,    1.0f, 0.0f,  0.0f,  1.0f}},
        {{1.0f, 1.0f, 1.0f,    0.0f,  0.0f,  1.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.0f, 0.0f,    1.0f, 0.0f,  0.0f,  1.0f}},
        {{0.0f, 0.0f, 1.0f,    0.0f,  0.0f,  1.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.1f, 1.0f,    1.0f, 0.0f,  0.0f,  1.0f}},
        {{0.0f, 1.0f, 1.0f,    0.0f,  0.0f,  1.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.1f, 0.0f,    1.0f, 0.0f,  0.0f,  1.0f}}
    }},
    // Right (1,0,0)
    {{
        {{1.0f, 0.0f, 0.0f,    1.0f,  0.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.0f, 1.0f,    0.0f, 0.0f, -1.0f, -1.0f}},
        {{1.0f, 1.0f, 0.0f,    1.0f,  0.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.0f, 0.0f,    0.0f, 0.0f, -1.0f, -1.0f}},
        {{1.0f, 0.0f, 1.0f,    1.0f,  0.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.1f, 1.0f,    0.0f, 0.0f, -1.0f, -1.0f}},
        {{1.0f, 1.0f, 1.0f,    1.0f,  0.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.1f, 0.0f,    0.0f, 0.0f, -1.0f, -1.0f}}
    }},
    // Back (0,0,1)
    {{
        {{0.0f, 0.0f, 0.0f,    0.0f,  0.0f, -1.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.0f, 1.0f,    1.0f, 0.0f,  0.0f,  1.0f}},
        {{0.0f, 1.0f, 0.0f,    0.0f,  0.0f, -1.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.0f, 0.0f,    1.0f, 0.0f,  0.0f,  1.0f}},
        {{1.0f, 0.0f, 0.0f,    0.0f,  0.0f, -1.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.1f, 1.0f,    1.0f, 0.0f,  0.0f,  1.0f}},
        {{1.0f, 1.0f, 0.0f,    0.0f,  0.0f, -1.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.1f, 0.0f,    1.0f, 0.0f,  0.0f,  1.0f}}
    }},
    // Left (-1,0,0)
    {{
        {{0.0f, 0.0f, 1.0f,   -1.0f,  0.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.0f, 1.0f,    1.0f, 0.0f,  0.0f,  1.0f}},
        {{0.0f, 1.0f, 1.0f,   -1.0f,  0.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.0f, 0.0f,    1.0f, 0.0f,  0.0f,  1.0f}},
        {{0.0f, 0.0f, 0.0f,   -1.0f,  0.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.1f, 1.0f,    1.0f, 0.0f,  0.0f,  1.0f}},
        {{0.0f, 1.0f, 0.0f,   -1.0f,  0.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.1f, 0.0f,    1.0f, 0.0f,  0.0f,  1.0f}}
    }},
    // Up (0,1,0)
    {{
        {{0.0f, 1.0f, 0.0f,    0.0f,  1.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.1f, 1.0f,    0.0f, 0.0f, -1.0f, -1.0f}},
        {{0.0f, 1.0f, 1.0f,    0.0f,  1.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.1f, 0.0f,    0.0f, 0.0f, -1.0f, -1.0f}},
        {{1.0f, 1.0f, 0.0f,    0.0f,  1.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.2f, 1.0f,    0.0f, 0.0f, -1.0f, -1.0f}},
        {{1.0f, 1.0f, 1.0f,    0.0f,  1.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.2f, 0.0f,    0.0f, 0.0f, -1.0f, -1.0f}}
    }},
    // Down (0,-1,0)
    {{
        {{0.0f, 0.0f, 1.0f,    0.0f, -1.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.0f, 1.0f,    1.0f, 0.0f, 0.0f, 1.0f}},
        {{0.0f, 0.0f, 0.0f,    0.0f, -1.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.0f, 0.0f,    1.0f, 0.0f, 0.0f, 1.0f}},
        {{1.0f, 0.0f, 1.0f,    0.0f, -1.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.1f, 1.0f,    1.0f, 0.0f, 0.0f, 1.0f}},
        {{1.0f, 0.0f, 0.0f,    0.0f, -1.0f,  0.0f,    1.0f, 1.0f, 1.0f, 1.0f,    0.1f, 0.0f,    1.0f, 0.0f, 0.0f, 1.0f}}
    }}
}};

static const unsigned short FaceIndices[] = {0, 1, 2, 2, 1, 3};

static const Vector3i Faces[] = {
    Vector3i(0, 0, 1), Vector3i(1, 0, 0), Vector3i(0, 0, -1), Vector3i(-1, 0, 0), Vector3i(0, 1, 0), Vector3i(0, -1, 0)
};

enum BlockType {
    STONE,
    GRAVEL,
    DIRT,
    SAND,
    SNOW
};

class Block {

public:
    double density_;
    float light_;

    /// Position within the Chunk
    Vector3i position_;
    bool solid_;
    BlockType type_;

    Block();
    Block(Vector3i position);
    Block(int x, int y, int z);

    /// Return as string.
    String ToString() const;
};

