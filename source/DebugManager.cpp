//
//  DebugManager.cpp
//  Cubid
//
//  Created by Tuomo Syvänperä on 23.4.2015.
//

#include <Urho3D/Urho3D.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Engine/Console.h>
#include <Urho3D/Engine/DebugHud.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Drawable.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/UI/UI.h>

#include "DebugManager.h"
#include "CameraManager.h"

using namespace Urho3D;

DebugManager::DebugManager(Context* context) : Object(context) {
}

void DebugManager::Initialize() {
    SubscribeToEvent(E_KEYDOWN, HANDLER(DebugManager, HandleKeyDown));
}

void DebugManager::CleanUp() {
}

void DebugManager::HandleKeyDown(StringHash eventType, VariantMap& eventData) {
    using namespace KeyDown;

    int key = eventData[P_KEY].GetInt();

    if (key == KEY_ESC) {
        Console* console = GetSubsystem<Console>();

        if (console->IsVisible()) {
            console->SetVisible(false);
        } else {
            GetSubsystem<Engine>()->Exit();
        }
    } else if (key == KEY_TAB) {
        UI* ui = GetSubsystem<UI>();
        ui->GetCursor()->SetVisible(!ui->GetCursor()->IsVisible());
        GetSubsystem<Input>()->SetMouseGrabbed(!ui->GetCursor()->IsVisible());
    } else if (key == KEY_F1) {
        GetSubsystem<Console>()->Toggle();
    } else if (key == KEY_F2) {
        GetSubsystem<DebugHud>()->ToggleAll();
    } else if (key == KEY_C) {
        Node* cameraNode = GetSubsystem<CameraManager>()->GetCameraNode();
        LOGDEBUGF("Camera is at position = %s, rotation = %s", cameraNode->GetPosition().ToString().CString(), cameraNode->GetRotation().ToString().CString());
    }
}
