//
// MeshData.cpp
// Cubid
//
// Created by Tuomo Syvänperä on 30.4.2015.
//

#include <Urho3D/Urho3D.h>
#include <Urho3D/IO/Log.h>

#include "Block.h"
#include "Chunk.h"
#include "MeshData.h"

using namespace Urho3D;

MeshData::MeshData() {
}

unsigned long MeshData::NumVertices() {
    return meshVertexBuffer_.vertices_.Size();
}

unsigned long MeshData::NumIndices() {
    return meshIndexBuffer_.indices_.size();
}

void MeshData::AddVertexData(const Block& block, const std::array<float, 16>& vertexData, float lightValue) {
    MeshVertex vertex;

    vertex.position_ =  Vector3(vertexData[0], vertexData[1], vertexData[2]) + block.position_;
    vertex.normal_ = Vector3(vertexData[3], vertexData[4], vertexData[5]);
//    vertex.color_.FromHSV(vertexData[6] * block.density_, vertexData[7] * block.density_, vertexData[8] * block.density_, vertexData[9]);
    vertex.color_.r_ = vertexData[6] * lightValue;
    vertex.color_.g_ = vertexData[7] * lightValue;
    vertex.color_.b_ = vertexData[8] * lightValue;
    vertex.color_.a_ = vertexData[9];
    vertex.texCoord1_ = Vector2(vertexData[10], vertexData[11]);
    vertex.tangent_ = Vector4(vertexData[12], vertexData[13], vertexData[14], vertexData[15]);

    meshVertexBuffer_.vertices_.Push(vertex);
}

void MeshData::AddFaceIndicesWithOffset(unsigned short offset) {
    for (auto i = 0; i < 6; i++) {
        meshIndexBuffer_.indices_.push_back(FaceIndices[i] + offset);
    }
}

SharedPtr<VertexBuffer> MeshData::GetVertexBuffer(Context* context) {
    SharedPtr<VertexBuffer> vb(new VertexBuffer(context));

    meshVertexBuffer_.elementMask_ = MASK_POSITION|MASK_NORMAL|MASK_COLOR|MASK_TEXCOORD1|MASK_TANGENT;

    // Calculate tangents
//    GenerateTangents(vertexData_.data(), 12 * sizeof(float), indices_.data(), sizeof(unsigned short), 0, NumVertices(), 3 * sizeof(float), 6 * sizeof(float), 8 * sizeof(float));

    vb->SetShadowed(true);
    meshVertexBuffer_.WriteToVertexBuffer(*vb.Get());

    return vb;
}

SharedPtr<IndexBuffer> MeshData::GetIndexBuffer(Context* context) {
    SharedPtr<IndexBuffer> ib(new IndexBuffer(context));

    ib->SetShadowed(true);
    ib->SetSize(meshIndexBuffer_.indices_.size(), false);
    ib->SetData(meshIndexBuffer_.Data());

    return ib;
}

