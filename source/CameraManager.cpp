//
//  CameraManager.cpp
//  Cubid
//
//  Created by Tuomo Syvänperä on 23.4.2015.
//

#include <Urho3D/Urho3D.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/Engine/Console.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Core/CoreEvents.h>

#include "CameraManager.h"
#include "SceneManager.h"
#include "WorldManager.h"

using namespace Urho3D;

CameraManager::CameraManager(Context* context) : Object(context) {
}

void CameraManager::Initialize() {
    cameraNode_ = GetSubsystem<SceneManager>()->GetScene()->CreateChild("Camera");
    Camera* camera = cameraNode_->CreateComponent<Camera>();
    camera->SetFarClip(512.0f);
    cameraNode_->SetPosition(Vector3(85.07f, 104.0f, 63.4f));
    cameraNode_->SetRotation(Quaternion(-0.69f, -0.14f, 0.68f, -0.14f));
//    cameraNode_->SetPosition(Vector3(0, 140, 0));
//    cameraNode_->SetRotation(Quaternion(0.8f, 0.3f, 0.4f, -0.2f));
    worldManager_ = GetSubsystem<WorldManager>();
    chunkPosition_ = worldManager_->WorldToChunk(cameraNode_->GetPosition());
}

void CameraManager::CleanUp() {
}

void CameraManager::Update(float deltaTime) {
    MoveCamera(deltaTime);
}

Node* CameraManager::GetCameraNode() {
    return cameraNode_.Get();
}

bool CameraManager::RaycastFromCamera(float maxDistance, Vector3& hitPos, Vector3& hitNormal, Drawable*& hitDrawable) {
    hitDrawable = nullptr;

    UI* ui = GetSubsystem<UI>();
    IntVector2 pos = ui->GetCursorPosition();
    // Check the cursor is visible and there is no UI element in front of the cursor
    if (!ui->GetCursor()->IsVisible() || ui->GetElementAt(pos, true))
        return false;

    Graphics* graphics = GetSubsystem<Graphics>();
    Camera* camera = GetCameraNode()->GetComponent<Camera>();
    Ray cameraRay = camera->GetScreenRay((float)pos.x_ / graphics->GetWidth(), (float)pos.y_ / graphics->GetHeight());
    // Pick only geometry objects, not eg. zones or lights, only get the first (closest) hit
    PODVector<RayQueryResult> results;
    RayOctreeQuery query(results, cameraRay, RAY_TRIANGLE, maxDistance, DRAWABLE_GEOMETRY);
    GetSubsystem<SceneManager>()->GetScene()->GetComponent<Octree>()->RaycastSingle(query);
    if (results.Size()) {
        RayQueryResult& result = results[0];
        hitPos = result.position_;
        hitNormal = result.normal_;
        hitDrawable = result.drawable_;
        return true;
    }

    return false;
}

Vector3 CameraManager::GetCameraPosition() {
    return cameraNode_->GetPosition();
}

Vector3i CameraManager::GetCameraChunkPosition() {
    return worldManager_->WorldToChunk(cameraNode_->GetPosition());
}

void CameraManager::MoveCamera(float deltaTime) {
    Input* input = GetSubsystem<Input>();
    // Movement speed as world units per second
    const float MOVE_SPEED = 20.0f;
    // Mouse sensitivity as degrees per pixel
    const float MOUSE_SENSITIVITY = 0.1f;

    Console* console = GetSubsystem<Console>();
    bool consoleVisible = console->IsVisible();

    if (input->IsMouseGrabbed()) {
        // Use this frame's mouse motion to adjust camera node yaw and pitch. Clamp the pitch between -90 and 90 degrees
        IntVector2 mouseMove = input->GetMouseMove();
        yaw_ += MOUSE_SENSITIVITY * mouseMove.x_;
        pitch_ += MOUSE_SENSITIVITY * mouseMove.y_;
        pitch_ = Clamp(pitch_, -90.0f, 90.0f);

        // Construct new orientation for the camera scene node from yaw and pitch. Roll is fixed to zero
        cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));
    }

    bool cameraMoved = false;
    // Read WASD keys and move the camera scene node to the corresponding direction if they are pressed
    if (!consoleVisible) {
        if (input->GetKeyDown(KEY_W)) {
            cameraNode_->Translate(Vector3::FORWARD * MOVE_SPEED * deltaTime);
            cameraMoved = true;
        }
        if (input->GetKeyDown(KEY_S)) {
            cameraNode_->Translate(Vector3::BACK * MOVE_SPEED * deltaTime);
            cameraMoved = true;
        }
        if (input->GetKeyDown(KEY_A)) {
            cameraNode_->Translate(Vector3::LEFT * MOVE_SPEED * deltaTime);
            cameraMoved = true;
        }
        if (input->GetKeyDown(KEY_D)) {
            cameraNode_->Translate(Vector3::RIGHT * MOVE_SPEED * deltaTime);
            cameraMoved = true;
        }
        if (input->GetKeyDown(KEY_E)) {
            cameraNode_->Translate(Vector3::UP * MOVE_SPEED * deltaTime, TransformSpace::TS_WORLD);
            cameraMoved = true;
        }
        if (input->GetKeyDown(KEY_Q)) {
            cameraNode_->Translate(Vector3::DOWN * MOVE_SPEED * deltaTime, TransformSpace::TS_WORLD);
            cameraMoved = true;
        }
    }

    if (cameraMoved) {
        Vector3i newChunkPosition = worldManager_->WorldToChunk(cameraNode_->GetPosition());
        if (chunkPosition_ != newChunkPosition) {
            chunkPosition_ = newChunkPosition;

            VariantMap eventData;
            eventData["NewChunkPosition"] = chunkPosition_;
            SendEvent("CameraChangedChunk", eventData);
        }
    }
}
