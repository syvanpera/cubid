//
//  SceneManager.cpp
//  Cubid
//
//  Created by Tuomo Syvänperä on 23.4.2015.
//

#include <Urho3D/Urho3D.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Engine/Console.h>
#include <Urho3D/Engine/DebugHud.h>
#include <Urho3D/Graphics/DebugRenderer.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Skybox.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/UI/UI.h>

#include <Urho3D/Graphics/VertexBuffer.h>
#include <Urho3D/Graphics/IndexBuffer.h>
#include <Urho3D/Graphics/Geometry.h>
#include <Urho3D/Graphics/Tangent.h>
#include <Urho3D/IO/Log.h>

#include "SceneManager.h"

using namespace Urho3D;

SceneManager::SceneManager(Context* context) : Object(context) {
}

void SceneManager::Initialize() {
    ResourceCache* cache = GetSubsystem<ResourceCache>();

    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();
    scene_->CreateComponent<DebugRenderer>();

    /*
    Node* zoneNode = scene_->CreateChild("Zone");
    Zone* zone = zoneNode->CreateComponent<Zone>();
    zone->SetBoundingBox(BoundingBox(-1000.0f, 1000.0f));
    zone->SetAmbientColor(Color(0.15f, 0.15f, 0.15f));
    zone->SetFogColor(Color(0.5f, 0.5f, 0.7f));
    zone->SetFogStart(100.0f);
    zone->SetFogEnd(300.0f);
    */

    Node* skyNode = scene_->CreateChild("Sky");
    skyNode->SetScale(500.0f);
    Skybox* skybox = skyNode->CreateComponent<Skybox>();
    skybox->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
    skybox->SetMaterial(cache->GetResource<Material>("Materials/Skybox.xml"));

    // Create a directional light to the world. Enable cascaded shadows on it
    Node* lightNode = scene_->CreateChild("DirectionalLight");
    lightNode->SetDirection(Vector3(0.6f, -1.0f, 0.8f));
    Light* light = lightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_DIRECTIONAL);
    light->SetCastShadows(true);
    light->SetShadowBias(BiasParameters(0.00025f, 0.5f));
    light->SetShadowCascade(CascadeParameters(10.0f, 50.0f, 200.0f, 0.0f, 0.8f));
    light->SetSpecularIntensity(0.5f);
    // Apply slightly overbright lighting to match the skybox
    light->SetColor(Color(1.2f, 1.2f, 1.2f));


    /*
    const unsigned numVertices = 8;

    float vertexData[] = {
        0.0f, 0.0f, 0.0f,    0.0f, 0.0f, -1.0f,    0.0f, 1.0f,    0.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,    0.0f, 0.0f, -1.0f,    0.0f, 0.0f,    0.0f, 0.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,    0.0f, 0.0f, -1.0f,    1.0f, 1.0f,    0.0f, 0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 0.0f,    0.0f, 0.0f, -1.0f,    1.0f, 0.0f,    0.0f, 0.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 1.0f,    1.0f, 0.0f,  0.0f,    1.0f, 0.0f,    0.0f, 0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f,    1.0f, 0.0f,  0.0f,    1.0f, 1.0f,    0.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f,    0.0f, 0.0f, -1.0f,    1.0f, 0.0f,    0.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 1.0f,    0.0f, 0.0f, -1.0f,    1.0f, 1.0f,    0.0f, 0.0f, 0.0f, 0.0f
    };

    const unsigned numIndices = 36;

    const unsigned short indexData[] = {
        0,  1,  2,
        2,  1,  3,
        2,  3,  4,
        4,  3,  5,
        4,  5,  6,
        6,  5,  7,
        6,  7,  0,
        0,  7,  1,
        1,  7,  3,
        3,  7,  5,
        6,  0,  4,
        4,  0,  2
    };

    GenerateTangents(vertexData, 12 * sizeof(float), indexData, sizeof(unsigned short), 0, numIndices, 3 * sizeof(float), 6 * sizeof(float), 8 * sizeof(float));

    for (auto i = 0; i < numVertices; i++) {
        LOGDEBUGF("VERTEX: Pos = %f,%f,%f  Normal = %f,%f,%f  UV = %f,%f  Tangent = %f,%f,%f,%f",
                  vertexData[(i*12)], vertexData[(i*12)+1], vertexData[(i*12)+2],
                  vertexData[(i*12)+3], vertexData[(i*12)+4], vertexData[(i*12)+5],
                  vertexData[(i*12)+6], vertexData[(i*12)+7],
                  vertexData[(i*12)+8], vertexData[(i*12)+9], vertexData[(i*12)+10], vertexData[(i*12)+11]);
    }

    SharedPtr<Model> fromScratchModel(new Model(context_));
    SharedPtr<VertexBuffer> vb(new VertexBuffer(context_));
    SharedPtr<IndexBuffer> ib(new IndexBuffer(context_));
    SharedPtr<Geometry> geom(new Geometry(context_));

    vb->SetShadowed(true);
    vb->SetSize(numVertices, MASK_POSITION|MASK_NORMAL|MASK_TEXCOORD1|MASK_TANGENT);
    vb->SetData(vertexData);

    ib->SetShadowed(true);
    ib->SetSize(numIndices, false);
    ib->SetData(indexData);

    geom->SetVertexBuffer(0, vb);
    geom->SetIndexBuffer(ib);
    geom->SetDrawRange(TRIANGLE_LIST, 0, numIndices);

    fromScratchModel->SetNumGeometries(1);
    fromScratchModel->SetGeometry(0, 0, geom);
    fromScratchModel->SetBoundingBox(BoundingBox(Vector3(0.0f, 0.0f, 0.0f), Vector3(1.0f, 1.0f, 1.0f)));

    Node* node = scene_->CreateChild("FromScratchObject");
    node->SetPosition(Vector3(-10.0f, 2.0f, 0.0f));
    StaticModel* object = node->CreateComponent<StaticModel>();
    object->SetModel(fromScratchModel);
    Material *mat = cache->GetResource<Material>("PurpleprintKit/Materials/Materials/Terrain/TerrainStone.xml");
//    mat->SetFillMode(FILL_WIREFRAME);
    object->SetMaterial(mat);
    */
}

void SceneManager::CleanUp() {
}

SharedPtr<Scene> SceneManager::GetScene() {
    return scene_;
}