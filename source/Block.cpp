//
//  Block.cpp
//  Cubid
//
//  Created by Tuomo Syvänperä on 21.4.2015.
//

#include <stdio.h>
#include <boost/format.hpp>

#include "Block.h"

Block::Block() : solid_(true), type_(STONE), light_(0.0f) {
}

Block::Block(Vector3i position) : Block() {
    position_ = position;
}

Block::Block(int x, int y, int z) : Block(Vector3i(x, y, z)) {
}

String Block::ToString() const {
    return String(str(boost::format("Block at %1%,%2%,%3%") % position_.x_ % position_.y_ % position_.z_).data());
}
