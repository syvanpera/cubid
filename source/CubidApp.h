//
//  CubidApp.h
//  Cubid
//
//  Created by Tuomo Syvänperä on 21.4.2015.
//

#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Engine/Application.h>

class DebugManager;
class UIManager;
class SceneManager;
class CameraManager;
class ChunkManager;
class WorldManager;

using namespace Urho3D;

class CubidApp : public Application {

public:
    CubidApp(Context* context);

    virtual void Setup();
    virtual void Start();
    virtual void Stop();

protected:

private:
    DebugManager* debugManager_;
    UIManager* uiManager_;
    SceneManager* sceneManager_;
    CameraManager* cameraManager_;
    WorldManager* worldManager_;

    void Initialize();
    void InitializeEngine();
    void InitializeSubsystems();
    void SetupScene();

    void InitViewport();
    void SubscribeToEvents();
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
};