//
//  Chunk.h
//  Cubid
//
//  Created by Tuomo Syvänperä on 21.4.2015.
//

#pragma once

#include <Urho3D/Scene/LogicComponent.h>

#include "Math/Vector3i.h"
#include "Block.h"

using namespace Urho3D;

class MeshData;

class Chunk : public LogicComponent {
    OBJECT(Chunk);

public:
    static const Vector3i Size;
    Vector3 worldPosition_;
    Vector3i gridPosition_;
    bool isDirty_ = true;

    Chunk(Context* context);

    virtual void Start();
    virtual void Stop();
    void GenerateChunk();
    void GenerateTerrain();
    void Render();

    Block* GetNeighbor(const Block& block, Vector3i direction);
    Block* GetBlockAt(Vector3i position);
    Block* GetBlockAt(int x, int y, int z);
    void RemoveBlockAt(const Vector3i& position);

    String ToString() const;

private:
    /// Blocks stored in [x][z][y] order
    Block*** blocks_;

    void CalculateLighting();
    void PropagateLight(Block& block, float lightValue, bool firstPass);
    MeshData GenerateMeshData();
    void FillMeshData(MeshData& meshData, const Block& block);
};
