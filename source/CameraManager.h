//
//  CameraManager.h
//  Cubid
//
//  Created by Tuomo Syvänperä on 23.4.2015.
//

#pragma once

#include "Math/Vector3i.h"
#include "WorldManager.h"

using namespace Urho3D;

namespace Urho3D {
    class Drawable;
}

class CameraManager : public Object {
    OBJECT(CameraManager);

public:
    CameraManager(Context* context);

    void Initialize();
    void CleanUp();
    void Update(float deltaTime);
    Node* GetCameraNode();
    bool RaycastFromCamera(float maxDistance, Vector3& hitPos, Vector3& hitNormal, Drawable*& hitDrawable);
    Vector3 GetCameraPosition();
    Vector3i GetCameraChunkPosition();

private:
    // Main Camera
    SharedPtr<Node> cameraNode_;
    // Camera yaw angle.
    float yaw_ = 0;
    // Camera pitch angle.
    float pitch_ = 0;
    // The chunk position of the camera
    Vector3i chunkPosition_;
    WorldManager* worldManager_;

    void MoveCamera(float deltaTime);
};