//
//  UIManager.cpp
//  Cubid
//
//  Created by Tuomo Syvänperä on 23.4.2015.
//

#include <Urho3D/Urho3D.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Engine/Console.h>
#include <Urho3D/Engine/DebugHud.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/UIEvents.h>
#include <Urho3D/UI/Text.h>

#include "UIManager.h"
#include "WorldManager.h"

using namespace Urho3D;

UIManager::UIManager(Context* context) : Object(context) {
}

void UIManager::Initialize() {
    // Get default style
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    defaultStyle_ = cache->GetResource<XMLFile>("UI/DefaultStyle.xml");

    UI* ui = GetSubsystem<UI>();
    ui->GetRoot()->SetDefaultStyle(defaultStyle_);

    SharedPtr<Cursor> cursor(new Cursor(context_));
    cursor->SetStyleAuto(defaultStyle_);
    cursor->SetVisible(true);
    ui->SetCursor(cursor);

    CreateFPSDisplay();
    CreateConsole();
    CreateDebugHud();
    CreateTerrainControls();
}

void UIManager::Update(float deltaTime) {
    timeSinceLastFPSUpdate_ += deltaTime;
    if (timeSinceLastFPSUpdate_ >= 1) {
        fpsText_->SetText("FPS: " + String(1.0 / deltaTime));
        timeSinceLastFPSUpdate_ = 0;
    }
}

void UIManager::CleanUp() {
}

void UIManager::CreateFPSDisplay() {
    Font* font = GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf");
    fpsText_ = new Text(context_);
    fpsText_->SetName("FPS Display");
    fpsText_->SetText("Getting FPS...");
    fpsText_->SetFont(font, 11);
    fpsText_->SetColor(Color::BLACK);

    GetSubsystem<UI>()->GetRoot()->AddChild(fpsText_);
}

void UIManager::CreateConsole() {
    Console* console = GetSubsystem<Engine>()->CreateConsole();
    console->SetDefaultStyle(defaultStyle_);
    console->GetBackground()->SetOpacity(0.8f);
}

void UIManager::CreateDebugHud() {
    DebugHud* debugHud = GetSubsystem<Engine>()->CreateDebugHud();
    debugHud->SetDefaultStyle(defaultStyle_);
}

void UIManager::CreateTerrainControls() {
    WorldManager* worldManager = GetSubsystem<WorldManager>();

    Window* window = CreateWindow();

    Slider* octaveSlider = CreateSlider(window, 200, 20, "Octaves");
    octaveSlider->SetRange(10.0f);
    octaveSlider->SetValue(worldManager->octaves_);
    SubscribeToEvent(octaveSlider, E_SLIDERCHANGED, HANDLER(UIManager, HandleOctaveChange));

    Slider* frequencySlider = CreateSlider(window, 200, 20, "Frequency");
    frequencySlider->SetRange(1.0f);
    frequencySlider->SetValue(worldManager->frequency_);
    SubscribeToEvent(frequencySlider, E_SLIDERCHANGED, HANDLER(UIManager, HandleFrequencyChange));

    Slider* persistenceSlider = CreateSlider(window, 200, 20, "Persistence");
    persistenceSlider->SetRange(1.0f);
    persistenceSlider->SetValue(worldManager->persistence_);
    SubscribeToEvent(persistenceSlider, E_SLIDERCHANGED, HANDLER(UIManager, HandlePersistenceChange));

    Slider* airThresholdSlider = CreateSlider(window, 200, 20, "Air Threshold");
    airThresholdSlider->SetRange(1.0f);
    airThresholdSlider->SetValue(worldManager->airThreshold_);
    SubscribeToEvent(airThresholdSlider, E_SLIDERCHANGED, HANDLER(UIManager, HandleAirThresholdChange));

    Button* button = CreateButton(window, 200, 40, "Regenerate Terrain");
    SubscribeToEvent(button, E_RELEASED, HANDLER(UIManager, HandleRegenerateTerrain));
}

Window* UIManager::CreateWindow() {
    // Create the Window and add it to the UI's root node
    Window* window = new Window(context_);
    GetSubsystem<UI>()->GetRoot()->AddChild(window);

    // Set Window size and layout settings
    window->SetMinSize(250, 300);
    window->SetLayout(LM_VERTICAL, 10, IntRect(10, 10, 10, 10));
    window->SetAlignment(HA_RIGHT, VA_TOP);
    window->SetName("Terrain Controls");

    // Create Window 'titlebar' container
    UIElement* titleBar = new UIElement(context_);
    titleBar->SetMinSize(0, 24);
    titleBar->SetVerticalAlignment(VA_TOP);
    titleBar->SetLayoutMode(LM_HORIZONTAL);

    // Create the Window title Text
    Font* font = GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf");
    Text* windowTitle = new Text(context_);
    windowTitle->SetName("WindowTitle");
    windowTitle->SetText("Terrain Controls");
    windowTitle->SetFont(font, 11);

    // Add the title text to the titlebar
    titleBar->AddChild(windowTitle);

    // Add the title bar to the Window
    window->AddChild(titleBar);

    // Apply styles
    window->SetStyleAuto();
    windowTitle->SetStyleAuto();

    return window;
}

Button* UIManager::CreateButton(UIElement* container, int xSize, int ySize, const String& text) {
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    Font* font = cache->GetResource<Font>("Fonts/Anonymous Pro.ttf");

    // Create the button and center the text onto it
    Button* button = container->CreateChild<Button>();
    button->SetStyleAuto();
    button->SetMinSize(xSize, ySize);

    Text* buttonText = button->CreateChild<Text>();
    buttonText->SetAlignment(HA_CENTER, VA_CENTER);
    buttonText->SetFont(font, 10);
    buttonText->SetText(text);

    return button;
}

Slider* UIManager::CreateSlider(UIElement* container, int xSize, int ySize, const String& text) {
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    Font* font = cache->GetResource<Font>("Fonts/Anonymous Pro.ttf");

    // Create text and slider below it
    Text* sliderText = container->CreateChild<Text>();
    sliderText->SetFont(font, 10);
    sliderText->SetText(text);

    Slider* slider = container->CreateChild<Slider>();
    slider->SetStyleAuto();
    slider->SetMinSize(xSize, ySize);
    // Use 0-1 range for controlling sound/music master volume
    slider->SetRange(1.0f);

    return slider;
}

void UIManager::HandleRegenerateTerrain(StringHash eventType, VariantMap& eventData) {
    LOGDEBUG("UIManager::RegenerateTerrain");
    GetSubsystem<WorldManager>()->RegenerateWorld();
}

void UIManager::HandleOctaveChange(StringHash eventType, VariantMap& eventData) {
    using namespace SliderChanged;

    float newValue = eventData[P_VALUE].GetFloat();
    LOGDEBUGF("UIManager::OctaveChange octaves = %f", newValue);
    GetSubsystem<WorldManager>()->octaves_ = static_cast<int>(newValue);
}

void UIManager::HandleFrequencyChange(StringHash eventType, VariantMap& eventData) {
    using namespace SliderChanged;

    float newValue = eventData[P_VALUE].GetFloat();
    LOGDEBUGF("UIManager::FrequencyChange frequency = %f", newValue);
    GetSubsystem<WorldManager>()->frequency_ = newValue;
}

void UIManager::HandlePersistenceChange(StringHash eventType, VariantMap& eventData) {
    using namespace SliderChanged;

    float newValue = eventData[P_VALUE].GetFloat();
    LOGDEBUGF("UIManager::PersistenceChange persistence = %f", newValue);
    GetSubsystem<WorldManager>()->persistence_ = newValue;
}

void UIManager::HandleAirThresholdChange(StringHash eventType, VariantMap& eventData) {
    using namespace SliderChanged;

    float newValue = eventData[P_VALUE].GetFloat();
    LOGDEBUGF("UIManager::AirThresholdChange airThreshold = %f", newValue);
    GetSubsystem<WorldManager>()->airThreshold_ = newValue;
}
