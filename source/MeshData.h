//
// MeshData.h
// Cubid
//
// Created by Tuomo Syvänperä on 30.4.2015.
//

#pragma once

#include <array>
#include <vector>

#include <Urho3D/Urho3D.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Graphics/IndexBuffer.h>
#include <Urho3D/Graphics/VertexBuffer.h>
#include <Urho3D/Math/Vector3.h>
#include <Urho3D/IO/Log.h>

#include "Math/Vector3i.h"
#include "Block.h"

using namespace Urho3D;

struct MeshVertex {
    Vector3 position_;
    Vector3 normal_;
    Color color_;
    Vector2 texCoord1_;
    Vector2 texCoord2_;
    Vector3 cubeTexCoord1_;
    Vector3 cubeTexCoord2_;
    Vector4 tangent_;
};

struct MeshVertexBuffer {
    unsigned elementMask_;
    Vector<MeshVertex> vertices_;

    void WriteToVertexBuffer(VertexBuffer& vb) {
        // TODO Support dynamic vertex buffer (vertexBuffer->IsDynamic() != dynamic_)
        if (vb.GetVertexCount() != vertices_.Size() || vb.GetElementMask() != elementMask_)
            vb.SetSize(vertices_.Size(), elementMask_, false);

        unsigned char* dest = (unsigned char*) vb.Lock(0, vertices_.Size(), true);

        if (dest) {
            for (auto i = 0; i < vertices_.Size(); ++i) {
                if (elementMask_ & MASK_POSITION) {
                    *((Vector3*)dest) = vertices_[i].position_;
                    dest += sizeof(Vector3);
                }
                if (elementMask_ & MASK_NORMAL) {
                    *((Vector3*)dest) = vertices_[i].normal_;
                    dest += sizeof(Vector3);
                }
                if (elementMask_ & MASK_COLOR) {
                    *((unsigned*)dest) = vertices_[i].color_.ToUInt();
                    dest += sizeof(unsigned);
                }
                if (elementMask_ & MASK_TEXCOORD1) {
                    *((Vector2*)dest) = vertices_[i].texCoord1_;
                    dest += sizeof(Vector2);
                }
                if (elementMask_ & MASK_TEXCOORD2) {
                    *((Vector2*)dest) = vertices_[i].texCoord2_;
                    dest += sizeof(Vector2);
                }
                if (elementMask_ & MASK_CUBETEXCOORD1) {
                    *((Vector3*)dest) = vertices_[i].cubeTexCoord1_;
                    dest += sizeof(Vector3);
                }
                if (elementMask_ & MASK_CUBETEXCOORD2) {
                    *((Vector3*)dest) = vertices_[i].cubeTexCoord2_;
                    dest += sizeof(Vector3);
                }
                if (elementMask_ & MASK_TANGENT) {
                    *((Vector4*)dest) = vertices_[i].tangent_;
                    dest += sizeof(Vector4);
                }
            }
        } else {
            LOGERROR("Failed to lock vertex buffer");
        }
        vb.Unlock();
    }
};

struct MeshIndexBuffer {
    std::vector<unsigned short> indices_;

    // TODO Change this to WriteToIndexBuffer as above for Vertex buffer
    unsigned short* Data() {
        return indices_.data();
    }
};

class MeshData {

public:
    MeshData();

    std::vector<float> vertexData_;
    std::vector<unsigned short> indices_;

    unsigned long NumVertices();
    unsigned long NumIndices();
    void AddVertexData(const Block& block, const std::array<float, 16>& vertexData, float lightValue);
    void AddFaceIndicesWithOffset(unsigned short offset);

    SharedPtr<VertexBuffer> GetVertexBuffer(Context* context);
    SharedPtr<IndexBuffer> GetIndexBuffer(Context* context);

private:
    MeshVertexBuffer meshVertexBuffer_;
    MeshIndexBuffer meshIndexBuffer_;

};
