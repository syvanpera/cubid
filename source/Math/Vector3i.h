//
// Vector3i.h
// Cubid
//
// Created by Tuomo Syvänperä on 29.4.2015.
//

#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Math/Vector3.h>

using namespace Urho3D;

class Vector3i {

public:
    /// X coordinate
    int x_;
    /// Y coordinate
    int y_;
    /// Z coordinate
    int z_;

    /// Construct a zero vector.
    Vector3i();
    /// Copy-construct from another vector.
    Vector3i(const Vector3i& vector);
    /// Construct from Vector3
    Vector3i(const Vector3& vector3);
    /// Construct from coordinates.
    Vector3i(int x, int y, int z);
    /// Construct from float coordinates.
    Vector3i(float x, float y, float z);
    /// Assign from another vector.
    Vector3i& operator = (const Vector3i& rhs);
    /// Add another vector
    Vector3i operator + (const Vector3i& rhs) const;
    /// Subtract another vector
    Vector3i operator - (const Vector3i& rhs) const;
    /// Test for equality with another vector without epsilon.
    bool operator == (const Vector3i& rhs) const;
    /// Test for inequality with another vector without epsilon.
    bool operator != (const Vector3i& rhs) const;
    /// Type-cast to Vector3
    operator Vector3() const;

    /// Zero vector.
    static const Vector3i ZERO;
    /// (-1,0,0) vector.
    static const Vector3i LEFT;
    /// (1,0,0) vector.
    static const Vector3i RIGHT;
    /// (0,1,0) vector.
    static const Vector3i UP;
    /// (0,-1,0) vector.
    static const Vector3i DOWN;
    /// (0,0,1) vector.
    static const Vector3i FORWARD;
    /// (0,0,-1) vector.
    static const Vector3i BACK;
    /// (1,1,1) vector.
    static const Vector3i ONE;

    String ToString() const;
};
