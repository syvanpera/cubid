//
// Vector3i.cpp
// Cubid
//
// Created by Tuomo Syvänperä on 29.4.2015.
//

#include <boost/format.hpp>

#include "Vector3i.h"

const Vector3i Vector3i::ZERO;
const Vector3i Vector3i::LEFT(-1, 0, 0);
const Vector3i Vector3i::RIGHT(1, 0, 0);
const Vector3i Vector3i::UP(0, 1, 0);
const Vector3i Vector3i::DOWN(0, -1, 0);
const Vector3i Vector3i::FORWARD(0, 0, 1);
const Vector3i Vector3i::BACK(0, 0, -1);
const Vector3i Vector3i::ONE(1, 1, 1);

Vector3i::Vector3i() :
        x_(0),
        y_(0),
        z_(0) {}

Vector3i::Vector3i(const Vector3i& vector) :
        x_(vector.x_),
        y_(vector.y_),
        z_(vector.z_) {}

Vector3i::Vector3i(const Vector3& vector) :
        x_(static_cast<int>(vector.x_)),
        y_(static_cast<int>(vector.y_)),
        z_(static_cast<int>(vector.z_)) {}

Vector3i::Vector3i(int x, int y, int z) :
        x_(x),
        y_(y),
        z_(z) {}

Vector3i::Vector3i(float x, float y, float z) :
    x_(static_cast<int>(x)),
    y_(static_cast<int>(y)),
    z_(static_cast<int>(z)) {}

Vector3i& Vector3i::operator = (const Vector3i& rhs) {
    x_ = rhs.x_;
    y_ = rhs.y_;
    z_ = rhs.z_;

    return *this;
}

Vector3i Vector3i::operator + (const Vector3i& rhs) const {
    return Vector3i(x_ + rhs.x_, y_ + rhs.y_, z_ + rhs.z_);
}

Vector3i Vector3i::operator - (const Vector3i& rhs) const {
    return Vector3i(x_ - rhs.x_, y_ - rhs.y_, z_ - rhs.z_);
}

bool Vector3i::operator == (const Vector3i& rhs) const {
    return x_ == rhs.x_ && y_ == rhs.y_ && z_ == rhs.z_;
}

bool Vector3i::operator != (const Vector3i& rhs) const {
    return x_ != rhs.x_ || y_ != rhs.y_ || z_ != rhs.z_;
}

Vector3i::operator Vector3() const {
    return Vector3(x_, y_, z_);
}

String Vector3i::ToString() const {
    return String(str(boost::format("Vector3i (%1%,%2%,%3%)") % x_ % y_ % z_).data());
}
