//
//  SceneManager.h
//  Cubid
//
//  Created by Tuomo Syvänperä on 23.4.2015.
//

#pragma once

#include <Urho3D/Scene/Scene.h>

using namespace Urho3D;

class SceneManager : public Object {
    OBJECT(SceneManager);
    
public:
    SceneManager(Context* context);
    
    void Initialize();
    void CleanUp();
    SharedPtr<Scene> GetScene();
    
private:
    // Main Scene
    SharedPtr<Scene> scene_;
};