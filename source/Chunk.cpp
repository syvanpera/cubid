//
//  Chunk.cpp
//  Cubid
//
//  Created by Tuomo Syvänperä on 21.4.2015.
//

#include <array>
#include <boost/format.hpp>
#include <noise/noise.h>

#include <Urho3D/Urho3D.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Graphics/Geometry.h>
#include <Urho3D/Graphics/IndexBuffer.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/VertexBuffer.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/IO/Log.h>

#include "Math/Vector3i.h"
#include "Block.h"
#include "Chunk.h"
#include "MeshData.h"
#include "WorldManager.h"


const Vector3i Chunk::Size = Vector3i(16, 128, 16);

Chunk::Chunk(Context* context) : LogicComponent(context),
                                 worldPosition_(Vector3()),
                                 gridPosition_(Vector3i()) {
}

void Chunk::Start() {
    worldPosition_ = node_->GetPosition();

    blocks_ = new Block**[Size.x_];
    for (auto x = 0; x < Size.x_; x++) {
        blocks_[x] = new Block*[Size.z_];
        for (auto z = 0; z < Size.z_; z++) {
            blocks_[x][z] = new Block[Size.y_];
            for (auto y = 0; y < Size.y_; y++) {
                blocks_[x][z][y].position_ = Vector3(x, y, z);
                blocks_[x][z][y].type_ = static_cast<BlockType>(Random(5));
            }
        }
    }
}

void Chunk::Stop() {
    for (auto x = 0; x < Size.x_; x++) {
        for (auto z = 0; z < Size.z_; z++) {
            delete[] blocks_[x][z];
        }
        delete[] blocks_[x];
    }
    delete[] blocks_;
}

void Chunk::GenerateChunk() {
    GenerateTerrain();
//    clock_t t = clock();
    CalculateLighting();
//    t = clock() - t;
//    LOGDEBUGF("Light calculation took %f ms", (static_cast<float>(t)/CLOCKS_PER_SEC)*1000);
}

void Chunk::GenerateTerrain() {
    WorldManager* worldManager = GetSubsystem<WorldManager>();
    noise::module::Perlin noise;
    noise.SetOctaveCount(worldManager->octaves_);
    noise.SetFrequency(worldManager->frequency_ / 100);
    noise.SetPersistence(worldManager->persistence_);
    noise.SetNoiseQuality(noise::QUALITY_BEST);
    noise.SetSeed(12345);

    for (auto x = 0; x < Size.x_; x++) {
        for (auto z = 0; z < Size.z_; z++) {
            int maxHeight = 0;
            for (auto y = 0; y < Size.y_; y++) {
                double value = noise.GetValue(worldPosition_.x_ + x, worldPosition_.y_ + y, worldPosition_.z_ + z);
                Block* block = &blocks_[x][z][y];
                block->density_ = value;
                // value = ((input - inputlow) / (inputhigh - inputlow)) * (outputhigh - outputlow) + outputlow
                value = (value + 1) * (Chunk::Size.y_/2);
//                if (value < worldManager->airThreshold_) {
                block->solid_ = (y <= value);
            }
        }
    }
}

void Chunk::CalculateLighting() {
    // Full light for all sunlit blocks
    for (auto x = 0; x < Size.x_; x++) {
        for (auto z = 0; z < Size.z_; z++) {
            for (auto y = Size.y_-1; y >= 0; y--) {
                Block* block = &blocks_[x][z][y];
                if (block->solid_) {
                    break;
                }
                block->light_ = 1.0f;
            }
        }
    }


    for (auto x = 0; x < Size.x_; x++) {
        for (auto z = 0; z < Size.z_; z++) {
            for (auto y = Size.y_-1; y >= 0; y--) {
                PropagateLight(blocks_[x][z][y], blocks_[x][z][y].light_, true);
            }
        }
    }
}

void Chunk::PropagateLight(Block& block, float lightValue, bool firstPass = false) {
    if (lightValue < 0.05 || block.solid_ || (block.light_ >= lightValue && !firstPass)) {
        return;
    }

    block.light_ = lightValue;

    for (auto i = 0; i < 6; i++) {
        Block* neighbor = GetNeighbor(block, Faces[i]);
        if (neighbor) {
            PropagateLight(*neighbor, 0.8f * lightValue);
        }
    }
}

void Chunk::Render() {
    if (isDirty_) {
        node_->RemoveComponent<StaticModel>();
        MeshData meshData = GenerateMeshData();

        SharedPtr<Model> chunkModel(new Model(context_));
        SharedPtr<VertexBuffer> vb(meshData.GetVertexBuffer(context_));
        SharedPtr<IndexBuffer> ib(meshData.GetIndexBuffer(context_));
        SharedPtr<Geometry> geom(new Geometry(context_));

        geom->SetVertexBuffer(0, vb);
        geom->SetIndexBuffer(ib);
        geom->SetDrawRange(TRIANGLE_LIST, 0, meshData.NumIndices());

        chunkModel->SetNumGeometries(1);
        chunkModel->SetGeometry(0, 0, geom);
        chunkModel->SetBoundingBox(BoundingBox(Vector3(0, 0, 0), Vector3(Size.x_, Size.y_, Size.z_)));

        StaticModel* chunkObject = node_->CreateComponent<StaticModel>();
        chunkObject->SetModel(chunkModel);
//        Material *material = GetSubsystem<WorldManager>()->materials_.At(Random(1));
        Material *material = SharedPtr<Material>(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/TerrainAtlas.xml"));
//        Material *material = SharedPtr<Material>(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Cubid.xml"));
//        Material *material = SharedPtr<Material>(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Stone.xml"));
//        material->SetFillMode(FILL_WIREFRAME);
        chunkObject->SetMaterial(material);

        isDirty_ = false;
    }
}

Block* Chunk::GetNeighbor(const Block& block, Vector3i direction) {
    Vector3i neighborPosition = static_cast<Vector3>(block.position_) + direction;

    Block* neighbor = GetBlockAt(neighborPosition);

    return neighbor;
}

Block* Chunk::GetBlockAt(Vector3i position) {
    return GetBlockAt(position.x_, position.y_, position.z_);
}

Block* Chunk::GetBlockAt(int x, int y, int z) {
    if (x >= 0 && x < Size.x_ && y >= 0 && y < Size.y_ && z >= 0 && z < Size.z_) {
        return &blocks_[x][z][y];
    }

    return nullptr;
}

void Chunk::RemoveBlockAt(const Vector3i& position) {
    LOGDEBUGF("Removing block at %s", position.ToString().CString());
    GetBlockAt(position)->solid_ = false;
    // TODO Don't mark the chunk dirty at this point. If there are multiple blocks to remove, you should only mark it dirty after all of them are removed
    isDirty_ = true;
}

MeshData Chunk::GenerateMeshData() {
//    clock_t t = clock();
    MeshData meshData;
    for (auto x = 0; x < Size.x_; x++) {
        for (auto z = 0; z < Size.z_; z++) {
            for (auto y = 0; y < Size.y_; y++) {
                if (blocks_[x][z][y].solid_) {
                    FillMeshData(meshData, blocks_[x][z][y]);
                }
            }
        }
    }
//    t = clock() - t;
//    LOGDEBUGF("Mesh data generation took %f ms", (static_cast<float>(t)/CLOCKS_PER_SEC)*1000);

    return meshData;
}

void Chunk::FillMeshData(MeshData& meshData, const Block& block) {
    for (auto i = 0; i < 6; i++) {
        Block* neighbor = GetNeighbor(block, Faces[i]);
        if (!neighbor || !neighbor->solid_) {
            int indexOffset = meshData.NumVertices();
            for (auto j = 0; j < 4; j++) {
                float lightValue = 0.0f;
                // Light is the average of the light values of blocks connected to this vertex
                for (Vector3i dir : VertexConnectedBlocks[i][j]) {
                    Block* connectedBlock = GetBlockAt(block.position_+dir);
                    lightValue += (connectedBlock) ? connectedBlock->light_ : 0.0f;
                }
                meshData.AddVertexData(block, BlockVertexData[i][j], lightValue/4);
            }
            meshData.AddFaceIndicesWithOffset(indexOffset);
        }
    }
}

String Chunk::ToString() const {
    return String(str(boost::format("Chunk at %1%,%2%,%3%") % gridPosition_.x_ % gridPosition_.y_ % gridPosition_.z_).data());
}
