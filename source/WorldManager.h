//
//  WorldManager.h
//  Cubid
//
//  Created by Tuomo Syvänperä on 4.5.2015.
//

#pragma once

#include <queue>

#include <Urho3D/Graphics/Material.h>

#include "Chunk.h"

class CameraManager;

using namespace Urho3D;

class WorldManager : public Object {
    OBJECT(WorldManager);

public:
    static const int VISIBLE_RADIUS = 4;

    // Some good combinations:
    // Perlin: f = 0.02f, o = 4 (rolling smooth, low hills)
    float frequency_   = 0.65f;
    int   octaves_     = 4;
    float persistence_ = 0.45;

    float airThreshold_ = 0.5;

    Vector<SharedPtr<Material>> materials_;

    WorldManager(Context* context);

    void Initialize();
    void Update(float deltaTime);
    void CleanUp();

    SharedPtr<Node> GetRootNode();
    void GenerateWorld();
    void RegenerateWorld();
    Vector3i WorldToBlock(Chunk* chunk, Vector3 worldPosition);
    Vector3i WorldToChunk(Vector3 worldPosition);

private:
    unsigned frameCount = 0;
    SharedPtr<Node> rootNode_;
    CameraManager* cameraManager_;

    Vector<Vector3i> renderQueue_;
    Vector<Chunk*> updateQueue_;

    void HandleMouseButton(StringHash eventType, VariantMap& eventData);
    void HitSomething();
    void SubscribeToEvents();
    void HandleChunkVisibilityChange(StringHash eventType, VariantMap& eventData);
    void AddVisibleChunksToWorld();
    void UnloadAllChunks();
    void UnloadInvisibleChunks();
    void RequestChunkRender(Vector3i position);
    void RenderChunk(Vector3i position);
    void UpdateChunk(Chunk* chunk);
};