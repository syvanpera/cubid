//
//  UIManager.h
//  Cubid
//
//  Created by Tuomo Syvänperä on 23.4.2015.
//

#pragma once

#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/Slider.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/Window.h>

using namespace Urho3D;

class UIManager : public Object {
    OBJECT(UIManager);

public:
    UIManager(Context* context);

    void Initialize();
    void Update(float deltaTime);
    void CleanUp();

private:
    XMLFile* defaultStyle_;
    Text* fpsText_;
    float timeSinceLastFPSUpdate_ = 0;

    void CreateFPSDisplay();
    void CreateConsole();
    void CreateDebugHud();
    void CreateTerrainControls();
    Window* CreateWindow();
    Button* CreateButton(UIElement* container, int xSize, int ySize, const String& text);
    Slider* CreateSlider(UIElement* container, int xSize, int ySize, const String& text);
    void HandleRegenerateTerrain(StringHash eventType, VariantMap& eventData);
    void HandleOctaveChange(StringHash eventType, VariantMap& eventData);
    void HandleFrequencyChange(StringHash eventType, VariantMap& eventData);
    void HandlePersistenceChange(StringHash eventType, VariantMap& eventData);
    void HandleAirThresholdChange(StringHash eventType, VariantMap& eventData);
};