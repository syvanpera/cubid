//
//  CubidApp.cpp
//  Cubid
//
//  Created by Tuomo Syvänperä on 21.4.2015.
//

#include <Urho3D/Urho3D.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/IO/Log.h>

#include "CubidApp.h"
#include "DebugManager.h"
#include "UIManager.h"
#include "SceneManager.h"
#include "CameraManager.h"
#include "WorldManager.h"

DEFINE_APPLICATION_MAIN(CubidApp);

CubidApp::CubidApp(Context* context) : Application(context) {
}

void CubidApp::Setup() {
    InitializeEngine();
}

void CubidApp::Start() {
    InitializeSubsystems();

    Initialize();
}

void CubidApp::Stop() {
    debugManager_->CleanUp();
    uiManager_->CleanUp();
    sceneManager_->CleanUp();
    cameraManager_->CleanUp();
    worldManager_->CleanUp();

    engine_->DumpResources(true);
}

void CubidApp::InitializeEngine() {
    engineParameters_["WindowTitle"]        = "Cubid";
    engineParameters_["FullScreen"]         = false;
    engineParameters_["Headless"]           = false;
    engineParameters_["WindowWidth"]        = 1280;
    engineParameters_["WindowHeight"]       = 900;
    engineParameters_["ResourcePrefixPath"] = "/Users/tinimini/Documents/Xcode Projects/Cubid/resources";
    engineParameters_["ResourcePaths"]      = "Data;CoreData;Cubid";
}

void CubidApp::InitializeSubsystems() {
    context_->RegisterSubsystem(new DebugManager(context_));
    context_->RegisterSubsystem(new UIManager(context_));
    context_->RegisterSubsystem(new SceneManager(context_));
    context_->RegisterSubsystem(new CameraManager(context_));
    context_->RegisterSubsystem(new WorldManager(context_));

    debugManager_ = GetSubsystem<DebugManager>();
    uiManager_ = GetSubsystem<UIManager>();
    sceneManager_ = GetSubsystem<SceneManager>();
    cameraManager_ = GetSubsystem<CameraManager>();
    worldManager_ = GetSubsystem<WorldManager>();

    debugManager_->Initialize();
    uiManager_->Initialize();
    sceneManager_->Initialize();
    cameraManager_->Initialize();
    worldManager_->Initialize();
}

void CubidApp::Initialize() {
    InitViewport();
    SubscribeToEvents();

    SetupScene();
}

void CubidApp::InitViewport() {
    Renderer* renderer = GetSubsystem<Renderer>();

    // Set up a viewport to the Renderer subsystem so that the 3D scene can be seen
    SharedPtr<Viewport> viewport(new Viewport(context_, sceneManager_->GetScene(), cameraManager_->GetCameraNode()->GetComponent<Camera>()));
    renderer->SetViewport(0, viewport);
}

void CubidApp::SetupScene() {
    Node* worldNode = worldManager_->GetRootNode();
    sceneManager_->GetScene()->AddChild(worldNode);

    clock_t t = clock();
    worldManager_->GenerateWorld();
    t = clock() - t;
    LOGDEBUGF("World generation took %f seconds", static_cast<float>(t)/CLOCKS_PER_SEC);
}

void CubidApp::SubscribeToEvents() {
    SubscribeToEvent(E_UPDATE, HANDLER(CubidApp, HandleUpdate));
}

void CubidApp::HandleUpdate(StringHash eventType, VariantMap& eventData) {
    // Take the frame time step, which is stored as a float
    float deltaTime = eventData[Update::P_TIMESTEP].GetFloat();

    cameraManager_->Update(deltaTime);
    worldManager_->Update(deltaTime);
    uiManager_->Update(deltaTime);
}
