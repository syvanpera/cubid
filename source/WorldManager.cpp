//
//  WorldManager.cpp
//  Cubid
//
//  Created by Tuomo Syvänperä on 4.5.2015.
//

#include <boost/format.hpp>

#include <Urho3D/Urho3D.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Input/InputEvents.h>
#include <Urho3D/Graphics/Drawable.h>
#include <Urho3D/Resource/ResourceCache.h>

#include "Chunk.h"
#include "WorldManager.h"
#include "CameraManager.h"

using namespace Urho3D;

static const std::vector<Vector3i> VISIBLE_CHUNKS = {
    Vector3i( 0, 0,  0), Vector3i(-1, 0,  0), Vector3i( 0, 0, -1), Vector3i( 0, 0,  1), Vector3i( 1, 0,  0),
    Vector3i(-1, 0, -1), Vector3i(-1, 0,  1), Vector3i( 1, 0, -1), Vector3i( 1, 0,  1), Vector3i(-2, 0,  0),
    Vector3i( 0, 0, -2), Vector3i( 0, 0,  2), Vector3i( 2, 0,  0), Vector3i(-2, 0, -1), Vector3i(-2, 0,  1),
    Vector3i(-1, 0, -2), Vector3i(-1, 0,  2), Vector3i( 1, 0, -2), Vector3i( 1, 0,  2), Vector3i( 2, 0, -1),
    Vector3i( 2, 0,  1), Vector3i(-2, 0, -2), Vector3i(-2, 0,  2), Vector3i( 2, 0, -2), Vector3i( 2, 0,  2),
    Vector3i(-3, 0,  0), Vector3i( 0, 0, -3), Vector3i( 0, 0,  3), Vector3i( 3, 0,  0), Vector3i(-3, 0, -1),
    Vector3i(-3, 0,  1), Vector3i(-1, 0, -3), Vector3i(-1, 0,  3), Vector3i( 1, 0, -3), Vector3i( 1, 0,  3),
    Vector3i( 3, 0, -1), Vector3i( 3, 0,  1), Vector3i(-3, 0, -2), Vector3i(-3, 0,  2), Vector3i(-2, 0, -3),
    Vector3i(-2, 0,  3), Vector3i( 2, 0, -3), Vector3i( 2, 0,  3), Vector3i( 3, 0, -2), Vector3i( 3, 0,  2),
    Vector3i(-4, 0,  0), Vector3i( 0, 0, -4), Vector3i( 0, 0,  4), Vector3i( 4, 0,  0), Vector3i(-4, 0, -1),
    Vector3i(-4, 0,  1), Vector3i(-1, 0, -4), Vector3i(-1, 0,  4), Vector3i( 1, 0, -4), Vector3i( 1, 0,  4),
    Vector3i( 4, 0, -1), Vector3i( 4, 0,  1), Vector3i(-3, 0, -3), Vector3i(-3, 0,  3), Vector3i( 3, 0, -3),
    Vector3i( 3, 0,  3), Vector3i(-4, 0, -2), Vector3i(-4, 0,  2), Vector3i(-2, 0, -4), Vector3i(-2, 0,  4),
    Vector3i( 2, 0, -4), Vector3i( 2, 0,  4), Vector3i( 4, 0, -2), Vector3i( 4, 0,  2), Vector3i(-5, 0,  0),
    Vector3i(-4, 0, -3), Vector3i(-4, 0,  3), Vector3i(-3, 0, -4), Vector3i(-3, 0,  4), Vector3i( 0, 0, -5),
    Vector3i( 0, 0,  5), Vector3i( 3, 0, -4), Vector3i( 3, 0,  4), Vector3i( 4, 0, -3), Vector3i( 4, 0,  3),
    Vector3i( 5, 0,  0), Vector3i(-5, 0, -1), Vector3i(-5, 0,  1), Vector3i(-1, 0, -5), Vector3i(-1, 0,  5),
    Vector3i( 1, 0, -5), Vector3i( 1, 0,  5), Vector3i( 5, 0, -1), Vector3i( 5, 0,  1), Vector3i(-5, 0, -2),
    Vector3i(-5, 0,  2), Vector3i(-2, 0, -5), Vector3i(-2, 0,  5), Vector3i( 2, 0, -5), Vector3i( 2, 0,  5),
    Vector3i( 5, 0, -2), Vector3i( 5, 0,  2), Vector3i(-4, 0, -4), Vector3i(-4, 0,  4), Vector3i( 4, 0, -4),
    Vector3i( 4, 0,  4), Vector3i(-5, 0, -3), Vector3i(-5, 0,  3), Vector3i(-3, 0, -5), Vector3i(-3, 0,  5),
    Vector3i( 3, 0, -5), Vector3i( 3, 0,  5), Vector3i( 5, 0, -3), Vector3i( 5, 0,  3), Vector3i(-6, 0,  0),
    Vector3i( 0, 0, -6), Vector3i( 0, 0,  6), Vector3i( 6, 0,  0), Vector3i(-6, 0, -1), Vector3i(-6, 0,  1),
    Vector3i(-1, 0, -6), Vector3i(-1, 0,  6), Vector3i( 1, 0, -6), Vector3i( 1, 0,  6), Vector3i( 6, 0, -1),
    Vector3i( 6, 0,  1), Vector3i(-6, 0, -2), Vector3i(-6, 0,  2), Vector3i(-2, 0, -6), Vector3i(-2, 0,  6),
    Vector3i( 2, 0, -6), Vector3i( 2, 0,  6), Vector3i( 6, 0, -2), Vector3i( 6, 0,  2), Vector3i(-5, 0, -4),
    Vector3i(-5, 0,  4), Vector3i(-4, 0, -5), Vector3i(-4, 0,  5), Vector3i( 4, 0, -5), Vector3i( 4, 0,  5),
    Vector3i( 5, 0, -4), Vector3i( 5, 0,  4), Vector3i(-6, 0, -3), Vector3i(-6, 0,  3), Vector3i(-3, 0, -6),
    Vector3i(-3, 0,  6), Vector3i( 3, 0, -6), Vector3i( 3, 0,  6), Vector3i( 6, 0, -3), Vector3i( 6, 0,  3),
    Vector3i(-7, 0,  0), Vector3i( 0, 0, -7), Vector3i( 0, 0,  7), Vector3i( 7, 0,  0), Vector3i(-7, 0, -1),
    Vector3i(-7, 0,  1), Vector3i(-5, 0, -5), Vector3i(-5, 0,  5), Vector3i(-1, 0, -7), Vector3i(-1, 0,  7),
    Vector3i( 1, 0, -7), Vector3i( 1, 0,  7), Vector3i( 5, 0, -5), Vector3i( 5, 0,  5), Vector3i( 7, 0, -1),
    Vector3i( 7, 0,  1), Vector3i(-6, 0, -4), Vector3i(-6, 0,  4), Vector3i(-4, 0, -6), Vector3i(-4, 0,  6),
    Vector3i( 4, 0, -6), Vector3i( 4, 0,  6), Vector3i( 6, 0, -4), Vector3i( 6, 0,  4), Vector3i(-7, 0, -2),
    Vector3i(-7, 0,  2), Vector3i(-2, 0, -7), Vector3i(-2, 0,  7), Vector3i( 2, 0, -7), Vector3i( 2, 0,  7),
    Vector3i( 7, 0, -2), Vector3i( 7, 0,  2), Vector3i(-7, 0, -3), Vector3i(-7, 0,  3), Vector3i(-3, 0, -7),
    Vector3i(-3, 0,  7), Vector3i( 3, 0, -7), Vector3i( 3, 0,  7), Vector3i( 7, 0, -3), Vector3i( 7, 0,  3),
    Vector3i(-6, 0, -5), Vector3i(-6, 0,  5), Vector3i(-5, 0, -6), Vector3i(-5, 0,  6), Vector3i( 5, 0, -6),
    Vector3i( 5, 0,  6), Vector3i( 6, 0, -5), Vector3i( 6, 0,  5)
};


WorldManager::WorldManager(Context* context) : Object(context) {
}

void WorldManager::Initialize() {
    context_->RegisterFactory<Chunk>();
    cameraManager_ = GetSubsystem<CameraManager>();

    SubscribeToEvents();
}

void WorldManager::CleanUp() {
}

void WorldManager::Update(float deltaTime) {
    frameCount++;
    PODVector<Node*> chunks;
    GetRootNode()->GetChildrenWithComponent<Chunk>(chunks);

    // TODO Push dirty chunks to an update queue and go through that instead of all chunks
//    for (auto i = 0; i < chunks.Size(); ++i) {
//        if (chunks[i]->GetComponent<Chunk>()->isDirty_) {
//            chunks[i]->GetComponent<Chunk>()->Render();
//        }
//    }

    // Handle one chunk from render queue every even frame
    if (!renderQueue_.Empty() && frameCount % 2 == 0) {
        RenderChunk(renderQueue_.Back());
        renderQueue_.Pop();
    }

    // Handle one chunk from update queue every odd frame
    if (!updateQueue_.Empty() && frameCount % 2 != 0) {
        UpdateChunk(updateQueue_.Back());
        updateQueue_.Pop();
    }
}

SharedPtr<Node> WorldManager::GetRootNode() {
    if (!rootNode_) {
        rootNode_ = new Node(context_);
        rootNode_->SetName("World");
        rootNode_->SetPosition(Vector3(0, 0, 0));
    }

    return rootNode_;
}

void WorldManager::GenerateWorld() {
    UnloadAllChunks();
    AddVisibleChunksToWorld();
}

void WorldManager::RegenerateWorld() {
    PODVector<Node*> chunks;
    GetRootNode()->GetChildrenWithComponent<Chunk>(chunks);

    for (unsigned i = 0; i < chunks.Size(); ++i) {
        Chunk* chunk = chunks[i]->GetComponent<Chunk>();
        chunk->isDirty_ = true;
        updateQueue_.Insert(0, chunk);
        chunk->GenerateTerrain();
    }
}

// TODO This doesn't work
Vector3i WorldManager::WorldToBlock(Chunk* chunk, Vector3 worldPosition) {
    worldPosition = Vector3(static_cast<float>(round(worldPosition.x_)), static_cast<float>(round(worldPosition.y_)), static_cast<float>(round(worldPosition.z_)));

    return worldPosition - Vector3(chunk->worldPosition_.x_ / (Chunk::Size.x_/2), chunk->worldPosition_.y_ / (Chunk::Size.y_/2), chunk->worldPosition_.z_ / (Chunk::Size.z_/2));
}

Vector3i WorldManager::WorldToChunk(Vector3 worldPosition) {
    worldPosition.x_ = worldPosition.x_ - ((worldPosition.x_ < 0) ? Chunk::Size.x_/2 : -(Chunk::Size.x_/2));
    worldPosition.z_ = worldPosition.z_ - ((worldPosition.z_ < 0) ? Chunk::Size.z_/2 : -(Chunk::Size.z_/2));

    return Vector3i(static_cast<int>(worldPosition.x_ / Chunk::Size.x_), 0, static_cast<int>(worldPosition.z_ / Chunk::Size.z_));
}

void WorldManager::HandleMouseButton(StringHash eventType, VariantMap& eventData) {
    using namespace MouseButtonDown;

    int button = eventData[P_BUTTON].GetInt();

    if (button == MOUSEB_LEFT) {
        HitSomething();
    }
}

void WorldManager::HitSomething() {
    Vector3 hitPos, hitNormal;
    Drawable* hitDrawable;

    if (cameraManager_->RaycastFromCamera(250.0f, hitPos, hitNormal, hitDrawable)) {
        LOGDEBUGF("Ray hit (pos = %s, normal = %s, drawable = %s) Object = %s", hitPos.ToString().CString(), hitNormal.ToString().CString(), hitDrawable->GetNode()->GetName().CString(), hitDrawable->GetNode()->GetComponent<Chunk>()->ToString().CString());
        Chunk* hitChunk = hitDrawable->GetNode()->GetComponent<Chunk>();
        if (hitChunk) {
            Vector3 adjustedPos = hitPos - hitNormal * 0.5f;
            Vector3i blockPosition = WorldToBlock(hitChunk, adjustedPos);
            LOGDEBUGF("Block position: %s", blockPosition.ToString().CString());
            Block* hitBlock = hitChunk->GetBlockAt(blockPosition);
            if (hitBlock) {
                LOGDEBUGF("Hit: %s in %s", hitBlock->ToString().CString(), hitChunk->ToString().CString());
                hitChunk->RemoveBlockAt(blockPosition);
            }
        }
    }
}

void WorldManager::SubscribeToEvents() {
    SubscribeToEvent(E_MOUSEBUTTONDOWN, HANDLER(WorldManager, HandleMouseButton));
    SubscribeToEvent("CameraChangedChunk", HANDLER(WorldManager, HandleChunkVisibilityChange));
}

void WorldManager::HandleChunkVisibilityChange(StringHash eventType, VariantMap& eventData) {
    AddVisibleChunksToWorld();
    UnloadInvisibleChunks();
}

void WorldManager::AddVisibleChunksToWorld() {
    Vector3i currentChunk = cameraManager_->GetCameraChunkPosition();
    for (auto visiblePos : VISIBLE_CHUNKS) {
        RequestChunkRender(currentChunk + visiblePos);
    }
}

void WorldManager::UnloadInvisibleChunks() {
    // TODO Instead of this shenanigans, just use the distance of the chunk to the camera. If it's far away, then unload
    PODVector<Node*> chunks;
    GetRootNode()->GetChildrenWithComponent<Chunk>(chunks);
    Vector3i currentChunk  = cameraManager_->GetCameraChunkPosition();

    bool isVisible = false;
    for (auto i = 0; i < chunks.Size(); ++i) {
        for (Vector3i v : VISIBLE_CHUNKS) {
            if ((currentChunk + v).ToString() == chunks[i]->GetName()) {
                isVisible = true;
            }
        }
        if (!isVisible) {
            GetRootNode()->RemoveChild(chunks[i]);
        }
        isVisible = false;
    }
}

void WorldManager::RequestChunkRender(Vector3i position) {
    if (GetRootNode()->GetChild(position.ToString()) == nullptr && !renderQueue_.Contains(position)) {
        renderQueue_.Insert(0, position);
    }
}

void WorldManager::RenderChunk(Vector3i position) {
//    clock_t t = clock();
    Node* chunkNode = GetRootNode()->CreateChild(position.ToString());
    chunkNode->SetPosition(Vector3(position.x_ * Chunk::Size.x_ - Chunk::Size.x_ / 2, position.y_ * Chunk::Size.y_,
                                   position.z_ * Chunk::Size.z_ - Chunk::Size.z_ / 2));
    Chunk* chunk = chunkNode->CreateComponent<Chunk>();
    chunk->gridPosition_ = position;
    UpdateChunk(chunk);
//    t = clock() - t;
//    LOGDEBUGF("WorldManager::RenderChunk %f ms", (static_cast<float>(t) / CLOCKS_PER_SEC) * 1000);
}

void WorldManager::UpdateChunk(Chunk* chunk) {
//    clock_t t = clock();
    // TODO Don't generate and render the chunk in the same update cycle
    chunk->GenerateChunk();
    chunk->Render();
//    t = clock() - t;
//    LOGDEBUGF("WorldManager::UpdateChunk %f ms", (static_cast<float>(t) / CLOCKS_PER_SEC) * 1000);
}

void WorldManager::UnloadAllChunks() {
    PODVector<Node*> chunks;
    GetRootNode()->GetChildrenWithComponent<Chunk>(chunks);

    for (unsigned i = 0; i < chunks.Size(); ++i) {
        GetRootNode()->RemoveChild(chunks[i]);
    }
}
